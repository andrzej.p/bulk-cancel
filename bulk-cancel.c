#include <fcntl.h>
#include <getopt.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libusb-1.0/libusb.h>

#define min(a, b) ((a) < (b) ? (a) : (b))

static unsigned char ep1_data[256];
static unsigned char ep4_data[256];
static unsigned char ep7_data[256];
static int max_interf;
static int burst_size = 1;
static int continue_loop = 1;

static struct transfer {
	struct libusb_device_handle	*handle;
	unsigned char			ep;
	unsigned char			*data;
	unsigned int			timeout;
	int				interface;
} transfers[] = {
	{
		.ep = 0x81,
		.data = ep1_data,
		.timeout = 1,
	},
	{
		.ep = 0x84,
		.data = ep4_data,
		.timeout = 1,
	},
	{
		.ep = 0x87,
		.data = ep7_data,
		.timeout = 1,
	},
};

static int init_interface(struct transfer *t, struct libusb_device_handle *h)
{
	int e;

	t->handle = h;
	t->interface = (t->ep & 0xf) - 1;
	e = libusb_claim_interface(t->handle, t->interface);
	if (e < 0) {
		printf("Cannot claim Interface: %s\n", libusb_strerror(e));
		return -1;    
	}

	t->data[0] = 'H';
	t->data[1] = 'e';
	t->data[2] = 'l';
	t->data[3] = 'l';
	t->data[4] = 'o';

	return 0;
}

static void complete_transfer(struct libusb_transfer *transfer)
{
	libusb_free_transfer(transfer);
}

static int submit_transfer(struct transfer *t)
{
	struct libusb_transfer *transfer;
	int i;

	for (i = 0; i < burst_size; ++i) {
		transfer = libusb_alloc_transfer(0);
		if (!transfer)
			return -1;
		libusb_fill_bulk_transfer(transfer, t->handle, t->ep, t->data, 256, complete_transfer, NULL, t->timeout);
		if (libusb_submit_transfer(transfer))
			return -1;
	}
	return 0;
}

static void stop_main(int sig)
{
	continue_loop = 0;
}

int main(int argc, char *argv[])
{
	struct libusb_device_handle *handle = NULL;
	struct sigaction int_handler = {
		.sa_handler = stop_main,
	};
	struct timeval timeout = {
		.tv_sec = 0,
		.tv_usec = 0,
	};
	const char *devpath;
	int e, i = 0, opt, fd;

	while ((opt = getopt(argc, argv, "D:i:b:")) != -1) {
		switch (opt) {
		case 'D':
			devpath = optarg;
			break;
		case 'i':
			max_interf = atoi(optarg);
			break;
		case 'b':
			burst_size = atoi(optarg);
			break;
		default:
			exit(1);
		}
	}

	e = sigaction(SIGINT, &int_handler, 0);
	if (e < 0) {
		printf("failed setting signal handler\n");
		return 1;
	}

	fd = open(devpath, O_RDWR);
	if (fd < 0) {
		printf("cannot open %s\n", devpath);
		return 1;
	}

	e = libusb_init(NULL);
	if (e < 0) {    
		printf("failed to initialise libusb\n");    
		return 1;    
	}    

	e = libusb_wrap_sys_device(NULL, fd, &handle);
	if (e != 0) {
		printf("failed wrapping device %s\n", devpath);
		return 1;
	}

	e = libusb_set_auto_detach_kernel_driver(handle, 1);
	if (e != 0) {
		printf("libusb_set_auto_detach_kernel_driver() failed\n");
		libusb_close(handle);
		return 1;
	}

	for (i = 0; i < max_interf; ++i)
		if (init_interface(transfers + i, handle))
			return 1;

	while (continue_loop) {
		for (i = 0; i < max_interf; ++i)
			if (submit_transfer(transfers + i))
				break;

		e = libusb_handle_events_timeout_completed(NULL, &timeout, NULL);
		if (e < 0)
			break;
	}

	for (i = 0; i < max_interf; ++i)
		libusb_release_interface(handle, transfers[i].interface);

	libusb_close(handle);
	libusb_exit(NULL);
	close(fd);

	printf("END.\n");

	return 0;
}

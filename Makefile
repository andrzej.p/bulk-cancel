all:
	gcc -Wall -pedantic bulk-cancel.c -L /usr/local/lib -lusb-1.0 -g -o bulk-cancel

clean:
	rm -rf bulk-cancel
